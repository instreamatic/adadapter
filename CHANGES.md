# Changes history #

## 1.3.4 ##
- Added call statistic pixels

## 1.3.3 ##
- Switching to SDK triton by analyzing the ADSYSTEM field from VAST

## 1.3.2 ##
- Update adman version 8.3.0 (with ad-dialog)

## 1.2.0 ##
- Added processing of switching audio-focus

## 1.1.0 ##
- Added ad-voice

## 1.0.0 - Beta ##
- First beta version
