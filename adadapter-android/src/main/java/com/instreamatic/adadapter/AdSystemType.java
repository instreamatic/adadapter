package com.instreamatic.adadapter;

public enum AdSystemType {
    NONE,
    TRITON,
    ADMAN,
}
