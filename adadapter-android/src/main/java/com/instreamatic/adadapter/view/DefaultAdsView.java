package com.instreamatic.adadapter.view;

import android.app.Activity;

import com.instreamatic.adadapter.view.core.IAdAdapterViewBundleFactory;
import com.instreamatic.adadapter.view.generic.DefaultAdsViewBindFactory;


/**
 * This sample shows a custom UI implementation.
 *
 * - Ads must be loaded at each play request so the server can update the tracking URLs.
 * - No support will be given on custom ad playback.
 * - Make sure to call the tracking methods from AdUtil at the right place or the ads might not be paid.
 */
public class DefaultAdsView extends BaseAdsView {

    final private static String TAG = "DefaultAdsView";

    private IAdAdapterViewBundleFactory factory;

    public DefaultAdsView(Activity activity) {
        super(activity);
        factory = new DefaultAdsViewBindFactory();
    }

    @Override
    public IAdAdapterViewBundleFactory factory() {
        return factory;
    }

}
