package com.instreamatic.adadapter.view.core;

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.tritondigital.ads.SyncBannerView;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

public class AdAdapterViewBundle implements IAdAdapterViewBundle {
    private final Map<Integer, AdAdapterViewBind> byId = new HashMap();
    private final Map<AdAdapterViewType, AdAdapterViewBind> byType = new HashMap();

    public AdAdapterViewBundle(List<AdAdapterViewBind> bindings) {
        Iterator var2 = bindings.iterator();

        while(var2.hasNext()) {
            AdAdapterViewBind bind = (AdAdapterViewBind) var2.next();
            if (bind.view != null) {
                this.byId.put(bind.view.getId(), bind);
                this.byType.put(bind.type, bind);
            }
        }

    }

    public <T extends View> boolean contains(AdAdapterViewType<T> type) {
        return this.byType.containsKey(type) && ((AdAdapterViewBind)this.byType.get(type)).view != null;
    }

    public <T extends View> T get(AdAdapterViewType<T> type) {
        return this.byType.containsKey(type) ? (T) ((AdAdapterViewBind)this.byType.get(type)).view : null;
    }


    public AdAdapterViewBind getById(int id) {
        return (AdAdapterViewBind)this.byId.get(id);
    }

    public static AdAdapterViewBundle fromLayout(Activity activity, int layoutId, Map<AdAdapterViewType, Integer> bindings) {
        SyncBannerView view;
        LayoutInflater inflater = LayoutInflater.from(activity);
        ViewGroup container = (ViewGroup)inflater.inflate(layoutId, (ViewGroup)null, false);
        List<AdAdapterViewBind> result = new ArrayList();
        result.add(new AdAdapterViewBind(AdAdapterViewType.CONTAINER_AD, container));
        Iterator var6 = bindings.entrySet().iterator();

        while(var6.hasNext()) {
            Map.Entry<AdAdapterViewType, Integer> entry = (Map.Entry)var6.next();
            result.add(new AdAdapterViewBind((AdAdapterViewType)entry.getKey(), container.findViewById((Integer)entry.getValue())));
        }

        return new AdAdapterViewBundle(result);
    }
}
