package com.instreamatic.adadapter.view;

import android.app.Activity;
import android.content.res.Configuration;
import android.graphics.Point;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.util.Log;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import android.view.View;
import android.view.ViewGroup;

import com.instreamatic.adadapter.AdAdapter;
import com.instreamatic.adadapter.TritonAdapterEvent;
import com.instreamatic.adadapter.view.core.AdAdapterLayoutType;
import com.instreamatic.adadapter.view.core.AdAdapterViewType;
import com.instreamatic.adadapter.view.core.IAdAdapterViewBundle;
import com.instreamatic.adadapter.view.core.IAdAdapterViewBundleFactory;
import com.instreamatic.adman.ActionUtil;
import com.instreamatic.adman.event.EventDispatcher;
import com.instreamatic.player.IAudioPlayer;
import com.tritondigital.ads.Ad;
import com.tritondigital.ads.SyncBannerView;

import java.lang.ref.WeakReference;


/**
 * Shows how to display an on-demand ad.
 */
abstract class BaseAdsView implements
        MediaPlayer.OnCompletionListener, MediaPlayer.OnErrorListener {
    final private static String TAG = "AdsActivity";

    private AdAdapter adAdapter;
    private BaseAdsView.AdWrapEventListener adWrapEventListener = new BaseAdsView.AdWrapEventListener();

    protected WeakReference<Activity> contextRef;
    protected View view;
    protected ViewGroup target;
    private IAdAdapterViewBundle bundle;

    protected boolean displaying;

    private SyncBannerView mBannerView;
    private SurfaceView mVideoView;
    private SurfaceHolderCallback holderSurfaceCallback = new SurfaceHolderCallback();

    public BaseAdsView(Activity context) {
        displaying = false;
        setActivity(context);
    }

    public void bindAd(AdAdapter adAdapter) {
        this.adAdapter = adAdapter;
        EventDispatcher dispatcher = adAdapter.getTritonAdapter().getDispatcher();
        dispatcher.addListener(TritonAdapterEvent.TYPE, adWrapEventListener);
    }

    public void unbindAd() {
        if (this.adAdapter == null) return;
        this.done();
        this.adAdapter = null;
    }

    public View getView() {
        return this.bundle != null ? this.bundle.get(AdAdapterViewType.CONTAINER_AD) : null;
    }

    public void setTarget(ViewGroup var1) {
        this.target = target;
    }

    public ViewGroup getTarget() {
        if (this.target != null) {
            return this.target;
        } else {
            Activity context = this.contextRef.get();
            if(context == null) {
                Log.w(TAG, "Activity is null");
                return null;
            }
            return (ViewGroup) context.getWindow().getDecorView().findViewById(android.R.id.content);
        }
    }

    public void show() {
        Activity context = contextRef.get();
        if (context == null) {
            Log.w(TAG, "Ad loading ERROR: NULL ad");
            return;
        }
        final Bundle ad = this.adAdapter != null ? this.adAdapter.getTritonAdapter().getCurrentAdBundle() : null;
        if ((ad == null) || ad.isEmpty()) {
            Log.w(TAG, "Ad loading ERROR: NULL ad");
            return;
        }
        context.runOnUiThread(new Runnable() {
            public void run() {
                prepareView();
                displaying = displayContent(ad);
            }
        });

    }

    public void close() {
        Activity context = contextRef.get();
        if(context == null) return;

        if (displaying) {
            context.runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    View viewContainer = getView();
                    if (viewContainer != null) {
                        getTarget().removeView(viewContainer);
                        displaying = false;
                        viewContainer.destroyDrawingCache();
                    }
                    else {
                        Log.e(TAG, "View layout not found");
                    }
                }
            });
        }
    }

    public void rebuild() {

    }

    abstract IAdAdapterViewBundleFactory factory();


    public void setActivity(Activity activity){
        WeakReference<Activity> weakRef = this.contextRef;
        this.contextRef = new WeakReference<>(activity);
        if (weakRef != null) {
            weakRef.clear();
        }
    }

    public void pause() {

    }

    public void resume() {

    }

    public void done() {
        if (this.adAdapter == null) return;
        EventDispatcher dispatcher = this.adAdapter.getTritonAdapter().getDispatcher();
        dispatcher.removeListener(TritonAdapterEvent.TYPE, adWrapEventListener);
        this.clearAd();
        this.close();
    }

    private IAdAdapterViewBundle buildViewBundle() {
        Activity context = contextRef.get();
        if (context == null) {
            Log.i(TAG, "Activity is null");
            return null;
        }

        int orientation = context.getResources().getConfiguration().orientation;
        if (orientation == Configuration.ORIENTATION_PORTRAIT) {
            return factory().build(AdAdapterLayoutType.PORTRAIT, context);
        } else {
            return factory().build(AdAdapterLayoutType.LANDSCAPE, context);
        }
    }

    private void prepareView() {
        this.bundle = buildViewBundle();
        if (this.bundle == null){
            Log.e(TAG, "Layout has not built");
            return;
        }
        if (bundle.contains(AdAdapterViewType.BANNER_AD)) {
            bundle.get(AdAdapterViewType.BANNER_AD).setVisibility(View.INVISIBLE);
        }
        if (bundle.contains(AdAdapterViewType.VIDEO_AD)) {
            bundle.get(AdAdapterViewType.VIDEO_AD).setVisibility(View.INVISIBLE);
        }
    }

    private void displayBanner(final Bundle ad, Activity context) {
        mBannerView = (bundle != null && bundle.contains(AdAdapterViewType.BANNER_AD))
                ? bundle.get(AdAdapterViewType.BANNER_AD) : null;
        if (mBannerView == null) {
            return;
        }
        Point point = new Point();
        context.getWindowManager().getDefaultDisplay().getSize(point);
        point = mBannerView.getBestBannerSize(ad, point.x, point.y);
        if (point == null) {
            int w = 640;
            int h = 640;
            point = mBannerView.getBestBannerSize(ad, w, h);
        }
        if (point == null) {
            Log.w(TAG, "Banner size not found");
            return;
        }
        mBannerView.setBannerSize(point.x, point.y);
        mBannerView.setVisibility(View.VISIBLE);
    }

    private void displayVideo() {
        mVideoView = (bundle != null && bundle.contains(AdAdapterViewType.VIDEO_AD))
                ? bundle.get(AdAdapterViewType.VIDEO_AD) : null;
        if (mVideoView == null) {
            return;
        }
        final IAudioPlayer player = this.adAdapter != null ? this.adAdapter.getTritonAdapter().getМediaPlayer() : null;
        if (player != null) {
            SurfaceHolder holder = mVideoView.getHolder();
            holder.addCallback(holderSurfaceCallback);
            mVideoView.setVisibility(View.VISIBLE);
        }
    }

    protected boolean displayContent(final Bundle ad) {
        boolean isAdd = false;
        ViewGroup target = getTarget();
        if (target != null) {
            View viewContainer = getView();
            if (viewContainer != null) {
                target.addView(viewContainer, new ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT));
                isAdd = true;
            }
            else{
                Log.e(TAG, "View container not found");
            }
        }
        String mimeType = ad != null ? ad.getString(Ad.MIME_TYPE): null;
        if (mimeType == null) {
            Log.d(TAG, "Warning: No audio/video");
        } else if (mimeType.startsWith("video")) {
            displayVideo();
        } else {
            Activity context = this.contextRef.get();
            if (context != null) {
                displayBanner(ad, context);
                if (mBannerView != null) mBannerView.showAd(ad);
            }
        }
        return isAdd;
    }


    ///////////////////////////////////////////////////////////////////////////
    // Ads playback
    ///////////////////////////////////////////////////////////////////////////
    public void clearAd() {
        // Clear the previous banner content without destroying the view.
        if (mBannerView != null) {
            mBannerView.clearBanner();
            mBannerView = null;
        }
        // Cancel the video playback
        if (mVideoView != null) {
            mVideoView.setOnClickListener(null);
            mVideoView.setVisibility(View.GONE);
            mVideoView = null;
        }
    }

    @Override
    public void onCompletion(MediaPlayer mp) {
        Log.d(TAG, "Playback completed.");
        clearAd();
        close();
    }

    @Override
    public boolean onError(MediaPlayer mp, int what, int extra) {
        Log.d(TAG, "Playback error: " + what + '/' + extra);
        clearAd();
        close();
        return true;
    }

    private class SurfaceHolderCallback implements SurfaceHolder.Callback {
        public void surfaceChanged(SurfaceHolder surfaceholder, int i, int j, int k) {
            Log.d(TAG, "surfaceChanged called");

        }

        public void surfaceDestroyed(SurfaceHolder surfaceholder) {
            Log.d(TAG, "surfaceDestroyed called");
        }

        public void surfaceCreated(SurfaceHolder holder) {
            Log.d(TAG, "surfaceCreated called");
            final IAudioPlayer player = adAdapter != null ? adAdapter.getTritonAdapter().getМediaPlayer() : null;
            if (mVideoView != null && player != null) {
                mVideoView.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        Log.d(TAG, "Video clicked.");
                        final Bundle ad = adAdapter != null ? adAdapter.getTritonAdapter().getCurrentAdBundle() : null;
                        if ((ad == null) || ad.isEmpty()) {
                            Log.w(TAG, "Ad loading ERROR: NULL ad");
                            return;
                        }

                        //track video clicks
                        Ad.trackVideoClick(ad);

                        final String clickUrl = ad.getString(Ad.VIDEO_CLICK_THROUGH_URL);
                        Log.d(TAG, "Video clicked url: " + clickUrl);
                        if ((clickUrl == null) || clickUrl.isEmpty()) {
                            return;
                        }

                        Activity context = contextRef.get();
                        if (context == null) {
                            Log.i(TAG, "Activity is null");
                            return;
                        }

                        ActionUtil.openUrl(context, clickUrl);
                    }
                });
                MediaPlayer mMediaPlayer = player.getMediaPlayer();
                mMediaPlayer.setDisplay(mVideoView.getHolder());
            }
        }

    }

    private class AdWrapEventListener implements TritonAdapterEvent.Listener {
        @Override
        public void onTritonAdapterEvent(TritonAdapterEvent admanEvent) {
            TritonAdapterEvent.Type type = admanEvent.getType();
            Log.d(TAG,"onWrapEvent, type: " + type);
            switch (type) {
                case AD_PLAYER_PLAYING:
                    show();
                    break;
                case AD_PLAYER_COMPLETE:
                case AD_PLAYER_FAILED:
                    close();
                    break;
            }
        }
    }
}
