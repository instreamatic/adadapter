package com.instreamatic.adadapter.view.core;

import android.view.SurfaceView;
import android.view.View;
import android.view.ViewGroup;

import com.tritondigital.ads.SyncBannerView;

final public class AdAdapterViewType<T extends View> {
    public static final AdAdapterViewType<ViewGroup> CONTAINER_AD = new AdAdapterViewType(ViewGroup.class);
    public static final AdAdapterViewType<SyncBannerView> BANNER_AD = new AdAdapterViewType(SyncBannerView.class);
    public static final AdAdapterViewType<SurfaceView> VIDEO_AD = new AdAdapterViewType(SurfaceView.class);
    public final Class<T> type;

    private AdAdapterViewType(Class<T> type) {
        this.type = type;
    }
}
