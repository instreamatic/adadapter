package com.instreamatic.adadapter.view.core;

public enum AdAdapterLayoutType {
    LANDSCAPE,
    PORTRAIT;

    private AdAdapterLayoutType() {
    }
}
