package com.instreamatic.adadapter.view.core;

import android.view.View;

public class AdAdapterViewBind<T extends View> {
    public final AdAdapterViewType type;
    public final T view;

    public AdAdapterViewBind(AdAdapterViewType<T> type, T view) {
        this.type = type;
        this.view = view;
    }
}