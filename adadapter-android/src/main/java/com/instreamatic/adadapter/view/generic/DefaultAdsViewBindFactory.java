package com.instreamatic.adadapter.view.generic;

import android.app.Activity;

import com.instreamatic.adadapter.R;
import com.instreamatic.adadapter.view.core.AdAdapterViewBindFactory;
import com.instreamatic.adadapter.view.core.AdAdapterViewBundle;
import com.instreamatic.adadapter.view.core.AdAdapterViewType;
import com.instreamatic.adadapter.view.core.IAdAdapterViewBundle;

import java.util.HashMap;
import java.util.Map;

public class DefaultAdsViewBindFactory  extends AdAdapterViewBindFactory {
    final Map<AdAdapterViewType, Integer> bindings = new HashMap<AdAdapterViewType, Integer>() {{
        put(AdAdapterViewType.BANNER_AD, R.id.banner);
        put(AdAdapterViewType.VIDEO_AD, R.id.surfaceView);
    }};

    @Override
    protected IAdAdapterViewBundle buildPortrait(Activity activity) {
        return AdAdapterViewBundle.fromLayout(activity, R.layout.ads_custom, bindings);
    }

    @Override
    protected IAdAdapterViewBundle buildLandscape(Activity activity) {
        return AdAdapterViewBundle.fromLayout(activity, R.layout.ads_custom, bindings);
    }
}
