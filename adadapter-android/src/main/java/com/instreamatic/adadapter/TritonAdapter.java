package com.instreamatic.adadapter;

import android.content.Context;
import android.media.AudioAttributes;
import android.media.AudioFocusRequest;
import android.media.AudioManager;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;

import com.instreamatic.adman.event.ControlEvent;
import com.instreamatic.adman.event.EventDispatcher;
import com.instreamatic.core.android.net.XmlLoader;
import com.instreamatic.player.AudioPlayer;
import com.instreamatic.player.IAudioPlayer;
import com.instreamatic.vast.AdSelector;
import com.instreamatic.vast.VASTDispatcher;
import com.instreamatic.vast.VASTParser;
import com.instreamatic.vast.VASTPlayer;
import com.instreamatic.vast.model.VASTInline;
import com.tritondigital.ads.Ad;
import com.tritondigital.ads.AdLoader;
import com.tritondigital.ads.AdParser;
import com.tritondigital.ads.AdRequestBuilder;

import org.w3c.dom.Document;

import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.lang.ref.WeakReference;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

final public class TritonAdapter implements AudioManager.OnAudioFocusChangeListener {
    final private static String TAG = "TritonAdapter";

    private WeakReference<Context> contextRef;
    private EventDispatcher dispatcher;

    private boolean needAudioFocus = false;
    private boolean loading = false;
    private boolean startPlaying = false;
    private boolean playing = false;

    //Ad Triton
    //private AdLoaderListener tAdLoaderListener = new AdLoaderListener();
    //private AudioPlayerListener tAudioPlayerListener = new AudioPlayerListener();
    //private AdLoader tAdLoader;

    private AdRequestBuilder tAdRequestBuilder;
    private Bundle tAd;
    private ADMediaPlayer mediaPlayerAD;

    private VASTLoaderTask vastLoaderTask;

    public TritonAdapter(Context context) {
        this.contextRef = new WeakReference<>(context);
        this.dispatcher = new EventDispatcher();
    }

    public void start() {
        load(true);
    }

    public void preload() {
        load(false);
    }

    public void load(boolean startPlaying) {
        if (this.isPlaying()){
            Log.w(TAG, "Ad is already playing! Please complete the playback then request the new ad.");
            return;
        }
        this.startPlaying = startPlaying;
//        tAdLoader = new AdLoader();
//        tAdLoader.setListener(tAdLoaderListener);
//        loading = true;
//        tAdLoader.load(tAdRequestBuilder);

        reset();
        String adRequest = null;
        try {
            adRequest = tAdRequestBuilder != null ? tAdRequestBuilder.build() : null;
        } catch (IllegalArgumentException ex) {
            adRequest = null;
        }
        if (adRequest == null) {
            dispatcher.dispatch(new TritonAdapterEvent(TritonAdapterEvent.Type.AD_LOADED_ERROR, TAG, "Ad parameters request  FAILED"));
            return;
        }
        Log.d(TAG, "adRequest: " + adRequest);
        loading = true;
        vastLoaderTask = new VASTLoaderTask();
        if (android.os.Build.VERSION.SDK_INT < 11) {
            vastLoaderTask.execute(adRequest);
        } else {
            vastLoaderTask.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, adRequest);
        }

    }

    public void pause() {
        if (mediaPlayerAD != null) {
            mediaPlayerAD.pause();
        }
    }

    public void play() {
        if (mediaPlayerAD != null) {
            mediaPlayerAD.play();
        }
    }

    public void skip() {
        dispatcher.dispatch(new TritonAdapterEvent(TritonAdapterEvent.Type.SKIPPED, TAG));
        this.reset();
    }

    public void reset() {
        Log.d(TAG, "Reset");
        if (mediaPlayerAD != null) {
            mediaPlayerAD.dispose();
            mediaPlayerAD = null;
        }
//        if (tAdLoader != null) {
//            tAdLoader.cancel();
//            tAdLoader = null;
//        }
        if (vastLoaderTask != null) {
            vastLoaderTask.cancel(true);
            vastLoaderTask = null;
        }
        tAd = null;
        loading = false;
        playing = false;
    }

    public AdRequestBuilder getRequest() {
        return tAdRequestBuilder;
    }

    public AdRequestBuilder updateRequest(AdRequestBuilder adRequestBuilder) {
        Log.d(TAG, "Update ad request");
        tAdRequestBuilder = adRequestBuilder;
        return adRequestBuilder;
    }


    public boolean isPlaying() {
        return this.playing;
    }


    public boolean isActive() {
        return this.isPlaying() || (this.getCurrentAdBundle() != null);
    }

    public Bundle getCurrentAdBundle() {
        return tAd;
    }

    public EventDispatcher getDispatcher() {
        return dispatcher;
    }

    public IAudioPlayer getМediaPlayer() {
        return mediaPlayerAD != null ? mediaPlayerAD.audioPlayer : null;
    }

    public void setParameters(Bundle parameters) {
        if (parameters != null) {
            this.needAudioFocus = parameters.getBoolean("adman.need_audio_focus", this.needAudioFocus);
        }
    }

    private void startAd(Bundle ad, List<VASTInline> vastAds) {
        List<TritonAdapterEvent> dispatcherEvents = new ArrayList<>();
        if ((ad == null) || ad.isEmpty()) {
            dispatcherEvents.add(new TritonAdapterEvent(TritonAdapterEvent.Type.AD_NONE, TAG, "Ad loading EMPTY"));
        }
        Context context = contextRef.get();
        if (context == null) {
            dispatcherEvents.add(new TritonAdapterEvent(TritonAdapterEvent.Type.ERROR, TAG, "Context is null"));
        }
        if (dispatcherEvents.size() > 0) {
            loading = false;
            for (TritonAdapterEvent dispatcherEvent : dispatcherEvents) {
                dispatcher.dispatch(dispatcherEvent);
            }
            return;
        }
        VASTDispatcher vastDispatcher = null;
        if (vastAds != null && !vastAds.isEmpty()) {
            AdSelector adSelector = new AdSelector();
            adSelector.ad = vastAds.remove(0);
            vastDispatcher = new VASTDispatcher(adSelector);
        }
        tAd = ad;
        String mimeType = ad.getString(Ad.MIME_TYPE);
        Log.d(TAG, "Ad is mime_type: " + mimeType);
        if (mimeType != null && (mimeType.startsWith("audio") || mimeType.startsWith("video")) ){
            String mediaUrl = tAd.getString(Ad.URL);
            mediaPlayerAD = new ADMediaPlayer(context, mediaUrl, vastDispatcher, this.startPlaying);
        } else {
            dispatcher.dispatch(new TritonAdapterEvent(TritonAdapterEvent.Type.ERROR, TAG, "Unsupported ad mime: " + mimeType));
        }
    }

    public void setVolume(float volume) {
        if (this.mediaPlayerAD != null) {
            this.mediaPlayerAD.setVolume(volume);
        }
    }

    public void setVast(byte[] byteArray, boolean startPlaying) {
        AdParser adParser = new AdParser();
        Bundle ad = null;
        try {
            InputStream is = new ByteArrayInputStream(byteArray);
            ad = adParser.a(is);
        } catch (Exception e) {
            Log.e(TAG, "VAST a_parser exception", e);
        }

        List<VASTInline> vastAds = null;
        try {
            InputStream is = new ByteArrayInputStream(byteArray);
            Document document = XmlLoader.extractDocument(is);
            is.close();
            List vastList = VASTParser.parseDocument(document);
            vastAds = VASTInline.toVASTInline(vastList);
        } catch (Exception e) {
            Log.e(TAG, "VAST v_parser exception", e);
        }

        this.startPlaying = startPlaying;
        dispatcher.dispatch(new TritonAdapterEvent(TritonAdapterEvent.Type.AD_LOADED, TAG, ad));
        startAd(ad, vastAds);
    }

    @Override
    public void onAudioFocusChange(int focusChange) {
        String event = "";
        switch (focusChange) {
            case AudioManager.AUDIOFOCUS_LOSS:
                event = "AUDIOFOCUS_LOSS";
                skip();
                break;
            case AudioManager.AUDIOFOCUS_LOSS_TRANSIENT:
                event = "AUDIOFOCUS_LOSS_TRANSIENT";
                pause();
                //changeState(State.ERROR);
                break;
            case AudioManager.AUDIOFOCUS_LOSS_TRANSIENT_CAN_DUCK:
                event = "AUDIOFOCUS_LOSS_TRANSIENT_CAN_DUCK";
                setVolume(0.5f);
                break;
            case AudioManager.AUDIOFOCUS_GAIN:
                event = "AUDIOFOCUS_GAIN";
                dispatcher.dispatch(new ControlEvent(ControlEvent.Type.RESUME));
                setVolume(1.0f);
                break;
        }
        Log.d(TAG, "onAudioFocusChange: " + event + " (" + focusChange + ")");
    }

    private void requestAudioFocus() {
        Context context = contextRef.get();
        if (!needAudioFocus || (context == null)) return;
        AudioManager audioManager = (AudioManager) context.getSystemService(Context.AUDIO_SERVICE);
        if (audioManager != null) {
            Log.d(TAG, "requestAudioFocus");
            if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.O) {
                AudioAttributes mAudioAttributes =
                        new AudioAttributes.Builder()
                                .setUsage(AudioAttributes.USAGE_MEDIA)
                                .setContentType(AudioAttributes.CONTENT_TYPE_MUSIC)
                                .build();
                AudioFocusRequest mAudioFocusRequest =
                        new AudioFocusRequest.Builder(AudioManager.AUDIOFOCUS_GAIN_TRANSIENT)
                                .setAudioAttributes(mAudioAttributes)
                                .setAcceptsDelayedFocusGain(true)
                                .setOnAudioFocusChangeListener(this)
                                .build();
                audioManager.requestAudioFocus(mAudioFocusRequest);
            } else {
                audioManager.requestAudioFocus(this, AudioManager.STREAM_MUSIC, AudioManager.AUDIOFOCUS_GAIN_TRANSIENT);
            }
        }
    }

    private void abandonAudioFocus() {
        Context context = contextRef.get();
        if (!needAudioFocus || (context == null)) return;
        AudioManager audioManager = (AudioManager) context.getSystemService(Context.AUDIO_SERVICE);
        if (audioManager != null) {
            Log.d(TAG, "abandonAudioFocus");
            audioManager.abandonAudioFocus(this);
        }
    }

    private class ADMediaPlayer implements AudioPlayer.CompleteListener, AudioPlayer.ProgressListener, AudioPlayer.StateListener {
        public AudioPlayer audioPlayer;

        ADMediaPlayer(Context context, String mediaURL, VASTDispatcher dispatcher, boolean autoplay) {
            audioPlayer = dispatcher == null
                            ? new AudioPlayer(context, mediaURL, autoplay)
                            : new VASTPlayer(context, mediaURL, dispatcher, autoplay, null, null, null);
            audioPlayer.setName("AdPlayerTriton");
            audioPlayer.setCompleteListener(this);
            audioPlayer.setStateListener(this);
            audioPlayer.setProgressListener(this);
        }

        public void dispose() {
            if (audioPlayer != null) {
                audioPlayer.dispose();
                audioPlayer = null;
            }
        }

        public void setVolume(float volume) {
            if (this.audioPlayer != null) {
                this.audioPlayer.setVolume(volume);
            }
        }

        public void pause() {
            if (audioPlayer != null) {
                audioPlayer.pause();
            }
        }

        public void play() {
            if (audioPlayer != null) {
                audioPlayer.resume();
            }
        }

        public void onComplete() {
            Log.d(TAG, "AudioPlayer.onComplete");
        }

        public void onProgressChange(int position, int duration) {
            Log.d(TAG, "AudioPlayer.onProgressChange");
            dispatcher.dispatch(new TritonAdapterEvent(TritonAdapterEvent.Type.AD_PLAYER_PROGRESS, TAG));
        }

        public void onStateChange(AudioPlayer.State state) {
            Log.d(TAG, "AudioPlayer.onStateChange, state: " + state);
            switch (state) {
                case PREPARE: //
                    dispatcher.dispatch(new TritonAdapterEvent(TritonAdapterEvent.Type.AD_PLAYER_PREPARE, TAG));
                    break;
                case READY:
                    dispatcher.dispatch(new TritonAdapterEvent(TritonAdapterEvent.Type.AD_PLAYER_READY, TAG));
                    break;
                case PLAYING:
                    if (playing) {
                        dispatcher.dispatch(new TritonAdapterEvent(TritonAdapterEvent.Type.AD_PLAYER_PLAY, TAG));
                    } else {
                        playing = true;
                        loading = false;
                        requestAudioFocus();
                        dispatcher.dispatch(new TritonAdapterEvent(TritonAdapterEvent.Type.AD_PLAYER_PLAYING, TAG));
                    }
                    break;
                case PAUSED:
                    dispatcher.dispatch(new TritonAdapterEvent(TritonAdapterEvent.Type.AD_PLAYER_PAUSE, TAG));
                    break;
                case ERROR:
                    dispatcher.dispatch(new TritonAdapterEvent(TritonAdapterEvent.Type.AD_PLAYER_FAILED, TAG));
                    reset();
                    break;
                case STOPPED:
                    abandonAudioFocus();
                    dispatcher.dispatch(new TritonAdapterEvent(TritonAdapterEvent.Type.AD_PLAYER_COMPLETE, TAG));
                    reset();
                    break;
            }
        }
    }
 /*
    private class AdLoaderListener implements AdLoader.AdLoaderListener {
        @Override
        public void onAdLoaded(AdLoader adLoader, Bundle ad) {
            Log.d(TAG, "AdLoader.onAdLoaded");
            dispatcher.dispatch(new TritonAdapterEvent(TritonAdapterEvent.Type.AD_LOADED, TAG, ad));
            startAd(ad);
        }

        @Override
        public void onAdLoadingError(AdLoader adLoader, int errorCode) {
            Log.d(TAG, "AdLoader.onAdLoadingError");
            loading = false;
            dispatcher.dispatch(new TritonAdapterEvent(TritonAdapterEvent.Type.AD_LOADED_ERROR, TAG, "Ad loading FAILED: " + AdLoader.debugErrorToStr(errorCode)));
            reset();
        }
    }

/**/
    private void doAdLoaded(byte[] byteArray) {
        setVast(byteArray, startPlaying);
    }

    private void doAdError(int errorCode) {
        loading = false;
        dispatcher.dispatch(new TritonAdapterEvent(TritonAdapterEvent.Type.AD_LOADED_ERROR, TAG, "Ad loading FAILED: " + AdLoader.debugErrorToStr(errorCode)));
        reset();
    }

    private class VASTLoaderTask extends AsyncTask<String, Void, byte[] > {
        private volatile int mParseError;
        @Override
        protected byte[]  doInBackground(String... adRequests) {
            InputStream is = null;
            byte[] byteArray = null;
            try {
                if (adRequests[0] != null) {
                    if (adRequests[0].startsWith("http")) {
                        URL url = new URL(adRequests[0]);
                        is = url.openStream();
                        byteArray = XmlLoader.toByteArray(is);
                    } else {
                        byteArray = adRequests[0].getBytes();
                        //is = new ByteArrayInputStream(adRequests[0].getBytes());
                    }
                }

            } catch (java.net.UnknownHostException e) {
                Log.e(TAG, "Download exception", e);
                mParseError = AdLoader.ERROR_UNKNOWN_HOST;

            } catch (java.io.FileNotFoundException e) {
                // Server doesn't recognise the station
                Log.w(TAG, "Download exception: " + adRequests[0], e);
                mParseError = AdLoader.ERROR_UNKNOWN;

            } catch (Exception e) {
                // URL not starting with http
                mParseError = AdLoader.ERROR_UNKNOWN;
                Log.e(TAG, "VAST exception: " + adRequests[0], e);
            }
            return byteArray;
        }


        @Override
        //protected void onPostExecute(final InputStream is) {
        protected void onPostExecute(byte[] byteArray) {
            // Handle errors
            if (mParseError != 0) {
                doAdError(mParseError);
            } else if (byteArray == null) {
                doAdError(AdLoader.ERROR_NO_INVENTORY);
            } else {
                doAdLoaded(byteArray);
            }
        }
    }
}
