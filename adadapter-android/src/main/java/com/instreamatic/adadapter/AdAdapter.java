package com.instreamatic.adadapter;

import android.content.Context;
import android.os.Bundle;
import android.util.Log;

import com.instreamatic.adman.AdmanRequest;
import com.instreamatic.adman.Region;
import com.instreamatic.adman.Type;
import com.instreamatic.adman.event.AdmanEvent;
import com.instreamatic.adman.event.EventDispatcher;
import com.instreamatic.adman.event.RequestEvent;
import com.instreamatic.vast.model.VASTInline;

final public class AdAdapter {
    final private static String TAG = "AdAdapter";

    private EventDispatcher dispatcher;
    private TritonAdapter tritonAdapter;
    private AdmanAdapter admanAdapter;
    private AdmanRequest admanRequestDefault = new AdmanRequest.Builder()
            .setSiteId(1249)
            .setRegion(Region.GLOBAL)
            .setType(Type.VOICE)
            .build();

    private boolean startPlaying = false;
    private AdAdapter.AdmanEventListener admanEventListener = new AdAdapter.AdmanEventListener();
    private AdAdapter.TritonAdapterEventListener tritonAdapterEventListener = new AdAdapter.TritonAdapterEventListener();

    public AdSystemType adSystemType = AdSystemType.NONE;

    public AdAdapter(Context context) {
        this.dispatcher = new EventDispatcher();
        tritonAdapter = new TritonAdapter(context);
        tritonAdapter.getDispatcher().addListener(TritonAdapterEvent.TYPE, tritonAdapterEventListener);

        admanAdapter = new AdmanAdapter(context, admanRequestDefault);
        EventDispatcher dispatcher = admanAdapter.getDispatcher();
        dispatcher.addListener(AdmanEvent.TYPE, admanEventListener);
        dispatcher.addListener(RequestEvent.TYPE, admanEventListener);
    }

    public  void setAdSystemType(AdSystemType adSystemType) {
        this.adSystemType = adSystemType;
    }

    public void start() {
        Log.d(TAG, "start");
        this.load(adSystemType, true);
    }

    public void preload(){
        Log.d(TAG, "preload");
        this.load(adSystemType, false);
    }

    public void pause(){
        Log.d(TAG, "pause");
        if (tritonAdapter.isActive()) {
            tritonAdapter.pause();
        }
        if (admanAdapter.isActive()) {
            admanAdapter.pause();
        }
    }

    public void play() {
        Log.d(TAG, "play");
        if (admanAdapter.isActive() || admanAdapter.getCurrentAd() != null) {
            admanAdapter.play();
            return;
        }
        if (tritonAdapter.isActive() || tritonAdapter.getCurrentAdBundle() != null) {
            tritonAdapter.play();
        }
    }

    public void skip() {
        Log.d(TAG, "skip");
        tritonAdapter.skip();
        admanAdapter.skip();
    }

    public void reset() {
        Log.d(TAG, "reset");
        tritonAdapter.reset();
        admanAdapter.reset();
    }

    public boolean isPlaying() {
        return tritonAdapter.isPlaying() || admanAdapter.isPlaying();
    }

    public boolean isActive() {
        boolean activeTriton = tritonAdapter.isActive();
        boolean activeAdman = admanAdapter.isActive();
        if (activeTriton) {
            Log.d(TAG,"TritonAdapter is active");
        }
        if (tritonAdapter.isActive()) {
            Log.d(TAG,"AdmanAdapter is active");
        }
        return  activeTriton || activeAdman;
    }

    public TritonAdapter getTritonAdapter() {
        return tritonAdapter;
    }

    public AdmanAdapter getAdmanAdapter() {
        return admanAdapter;
    }

    public EventDispatcher getDispatcher() {
        return dispatcher;
    }

    public void setParameters(Bundle parameters) {
        tritonAdapter.setParameters(parameters);
        admanAdapter.setParameters(parameters);
    }

    public void addListener(AdAdapterEvent.Listener listener) {
        dispatcher.addListener(AdAdapterEvent.TYPE, listener);
    }

    public void removeListener(AdAdapterEvent.Listener listener) {
        dispatcher.removeListener(AdAdapterEvent.TYPE, listener);
    }

    private void load(AdSystemType type, boolean startPlaying) {
        if (this.isActive()) {
            return;
        }
        this.startPlaying = startPlaying;
        if (type == AdSystemType.ADMAN) {
            admanAdapter.load(this.startPlaying);
        } else if (type == AdSystemType.TRITON) {
            tritonAdapter.load(this.startPlaying);
        } else {
            admanAdapter.load(this.startPlaying);
        }
    }

    private class TritonAdapterEventListener implements TritonAdapterEvent.Listener {
        @Override
        public void onTritonAdapterEvent(TritonAdapterEvent event){
            Log.d(TAG, "onTritonAdapterEvent");
            switch (event.getType()) {
                case AD_LOADED:
                    dispatcher.dispatch(new AdAdapterEvent(AdAdapterEvent.Type.PREPARE, TAG, AdSystemType.TRITON));
                    //TODO: ??
                    //String adSystem = event.ad != null ? event.ad.getString(Ad.TITLE, "") : "";
                    //checkAdSystem(adSystem, event.byteArray, AdSystemType.TRITON);
                    break;
                case AD_NONE:
                    dispatcher.dispatch(new AdAdapterEvent(AdAdapterEvent.Type.NONE, TAG, AdSystemType.TRITON));
                    break;
                case AD_PLAYER_PREPARE:
                    break;
                case AD_PLAYER_READY:
                    dispatcher.dispatch(new AdAdapterEvent(AdAdapterEvent.Type.READY, TAG, AdSystemType.TRITON));
                    break;
                case AD_PLAYER_PLAYING:
                    dispatcher.dispatch(new AdAdapterEvent(AdAdapterEvent.Type.STARTED, TAG, AdSystemType.TRITON));
                    break;
                case ERROR:
                case AD_LOADED_ERROR:
                case AD_PLAYER_FAILED:
                    dispatcher.dispatch(new AdAdapterEvent(AdAdapterEvent.Type.FAILED, TAG, AdSystemType.TRITON, "onTritonAdapterEvent: "+event.getType()));
                    break;
                case AD_PLAYER_COMPLETE:
                    dispatcher.dispatch(new AdAdapterEvent(AdAdapterEvent.Type.COMPLETED, TAG, AdSystemType.TRITON));
                    break;
                case SKIPPED:
                    dispatcher.dispatch(new AdAdapterEvent(AdAdapterEvent.Type.SKIPPED, TAG, AdSystemType.ADMAN));
                    break;
            }
        }
    }

    protected void checkAdSystem(String value, final byte[] vastBytes, AdSystemType adSystemSource) {
        Runnable task = null;
        String adSystem = value.toLowerCase();
        String STR_AD_SYSTEM_ADMAN = "instreamatic";
        if (adSystemSource == AdSystemType.TRITON) {
            if( adSystem.contains(STR_AD_SYSTEM_ADMAN)) {
                task = new Runnable() {
                    public void run() {
                        tritonAdapter.reset();
                        admanAdapter.setVast(vastBytes, startPlaying);
                    }
                };
            }
        }
        else {
            if(!adSystem.contains(STR_AD_SYSTEM_ADMAN)) {
                task = new Runnable() {
                    public void run() {
                        admanAdapter.adman.reset();
                        tritonAdapter.setVast(vastBytes, startPlaying);
                    }
                };
            }
        }
        if(task != null) {
            Thread thread = new Thread(task);
            thread.start();
        }
    }

    private class AdmanEventListener implements AdmanEvent.Listener, RequestEvent.Listener {
        @Override
        public void onAdmanEvent(AdmanEvent event) {
            AdmanEvent.Type type = event.getType();
            Log.d(TAG, "onAdmanEvent: " + type);
            switch (type) {
                case PREPARE:
                    dispatcher.dispatch(new AdAdapterEvent(AdAdapterEvent.Type.PREPARE, TAG, AdSystemType.ADMAN));
                    break;
                case NONE:
                    dispatcher.dispatch(new AdAdapterEvent(AdAdapterEvent.Type.NONE, TAG, AdSystemType.ADMAN));
                    if (adSystemType == AdSystemType.NONE) load(AdSystemType.TRITON, startPlaying);
                    break;
                case FAILED:
                    dispatcher.dispatch(new AdAdapterEvent(AdAdapterEvent.Type.FAILED, TAG, AdSystemType.ADMAN));
                    if (adSystemType == AdSystemType.NONE) load(AdSystemType.TRITON, startPlaying);
                    break;
                case READY:
                    dispatcher.dispatch(new AdAdapterEvent(AdAdapterEvent.Type.READY, TAG, AdSystemType.ADMAN));
                    break;
                case STARTED:
                    dispatcher.dispatch(new AdAdapterEvent(AdAdapterEvent.Type.STARTED, TAG, AdSystemType.ADMAN));
                    break;
                case ALMOST_COMPLETE:
                    dispatcher.dispatch(new AdAdapterEvent(AdAdapterEvent.Type.ALMOST_COMPLETE, TAG, AdSystemType.ADMAN));
                    break;
                case COMPLETED:
                    dispatcher.dispatch(new AdAdapterEvent(AdAdapterEvent.Type.COMPLETED, TAG, AdSystemType.ADMAN));
                    break;
                case SKIPPED:
                    dispatcher.dispatch(new AdAdapterEvent(AdAdapterEvent.Type.SKIPPED, TAG, AdSystemType.ADMAN));
                    break;
            }
        }

        @Override
        public void onRequestEvent(RequestEvent event) {
            RequestEvent.Type type = event.getType();
            Log.d(TAG, "onRequestEvent: " + type);
            switch (type) {
                case SUCCESS:
                    VASTInline ad = event.ads != null && event.ads.size()>0 ? event.ads.get(0) : null;
                    checkAdSystem(ad != null ? ad.adSystem : "", event.byteArray, AdSystemType.ADMAN);
                    break;
            }
        }
    }

}
