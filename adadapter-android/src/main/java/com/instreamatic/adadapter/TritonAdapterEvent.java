package com.instreamatic.adadapter;

import android.os.Bundle;

import com.instreamatic.adman.event.BaseEvent;
import com.instreamatic.adman.event.EventListener;
import com.instreamatic.adman.event.EventType;


public class TritonAdapterEvent extends BaseEvent<TritonAdapterEvent.Type, TritonAdapterEvent.Listener> {

    final public static EventType<TritonAdapterEvent.Type, TritonAdapterEvent, Listener> TYPE = new EventType<Type, TritonAdapterEvent, Listener>("TritonAdapter") {
        @Override
        public void callListener(TritonAdapterEvent event, Listener listener) {
            listener.onTritonAdapterEvent(event);
        }
    };

    public enum Type {
//        PREPARE,
//        NONE,
//        FAILED,
//        READY,
//        STARTED,
//        ALMOST_COMPLETE,
//        COMPLETED,
//        SKIPPED,

        ERROR,
        AD_LOADED,
        AD_LOADED_ERROR,
        AD_NONE,
        AD_PLAYER_PREPARE,
        AD_PLAYER_READY,
        AD_PLAYER_PLAY,
        AD_PLAYER_PLAYING,
        AD_PLAYER_PAUSE,
        AD_PLAYER_PROGRESS,
        AD_PLAYER_FAILED,
        AD_PLAYER_COMPLETE,
        SKIPPED,
    }

    final public Bundle ad;
    final public String message;
    final public byte[] byteArray;

    public TritonAdapterEvent(TritonAdapterEvent.Type type, String sender, Bundle ad, String message, byte[] byteArray) {
        super(type, sender);
        this.ad = ad;
        this.message = message;
        this.byteArray = byteArray;
    }

    public TritonAdapterEvent(TritonAdapterEvent.Type type, String sender, Bundle ad, String message) {
        this(type, sender, ad, message, null);
    }

    public TritonAdapterEvent(Type type, String sender, Bundle ad) {
        this(type, sender, ad, null);
    }

    public TritonAdapterEvent(Type type, String sender, String message) {
        this(type, sender, null, message);
    }

    public TritonAdapterEvent(Type type, String sender) {
        this(type, sender, null, null);
    }

    @Override
    public EventType<Type, TritonAdapterEvent, Listener> getEventType() {
        return TYPE;
    }

    public interface Listener extends EventListener {
        public void onTritonAdapterEvent(TritonAdapterEvent event);
    }
}
