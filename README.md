AdAdapter is a wrapper that is used to combine Triton SDK and Instreamatic SDK. 
It�s still possible to work with each SDK separately. 

In order to use AdAdapter, do the following:

Download the package from the repo:

Add the following links to build.gradle

api 'com.squareup.okhttp3:okhttp:3.10.0'

//Instreamatic SDK:
api 'com.instreamatic:adman-android:' + project.ext.admanVersion + '@aar'
api 'com.instreamatic:adman-android-view:' + project.ext.admanVersion + '@aar'
api 'com.instreamatic:adman-android-voice:' + project.ext.admanVersion + '@aar'

//google libraries for Triton SDK
api 'com.google.android.gms:play-services-ads:11.6.0'
api 'com.google.android.gms:play-services-base:11.6.0'
api 'com.google.android.gms:play-services-analytics:11.6.0'

//Triton SDK
api 'com.instreamatic.triton:ads:' + project.ext.tritonVersion + '@aar'
api 'com.instreamatic.triton:player:' + project.ext.tritonVersion + '@aar'
api 'com.instreamatic.triton:streamingproxy:' + project.ext.tritonVersion + '@aar'
api 'com.instreamatic.triton:util:' + project.ext.tritonVersion + '@aar'

//AdAdapter
api 'com.instreamatic.adadapter:adadapter-android:' + project.ext.adadapterVersion + '@aar'


## Please find the full usage example below:
	https://bitbucket.org/instreamatic/adadapter.demo


# Links:
## Triton SDK: 
	https://github.com/tritondigital/android-sdk
	https://userguides.tritondigital.com/spc/moband/using_the_android_sdk.html?anchor=display-on-demand-over-app
	
## AdAdapter: 
	https://bitbucket.org/instreamatic/adadapter
	
## Instreamatic SDK: 
    https://help.instreamatic.com/dialogue-ads-for-android/untitled#slot

       

